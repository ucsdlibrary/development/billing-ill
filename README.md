# ILL Billing System

[![Code Climate](https://codeclimate.com/repos/559aaef8e30ba06bfd0001d9/badges/c7609d3879a4d9b01db0/gpa.svg)](https://codeclimate.com/repos/559aaef8e30ba06bfd0001d9/feed) [![Test Coverage](https://codeclimate.com/repos/559aaef8e30ba06bfd0001d9/badges/c7609d3879a4d9b01db0/coverage.svg)](https://codeclimate.com/repos/559aaef8e30ba06bfd0001d9/coverage) [![Dependency Status](https://gemnasium.com/ucsdlib/billing-ill.svg)](https://gemnasium.com/ucsdlib/billing-ill)

ILL Billing System is a Ruby on Rails web application which is created for processing and invoicing the Receivables, UCSD Recharges, UC Recharges, and Payables from the Interlibrary Loan Services in [UC San Diego Library](http://libraries.ucsd.edu/ "UC San Diego Library")

## Requirements

* Ruby 2.2.3+
* git

## Installation

```
$ git@github.com:ucsdlib/billing-ill.git
```

## Usage

1.Open project.

```
$ cd billing-ill
```

2.Checkout develop branch.

```
$ git checkout develop
```

3.Copy DB Sample.

```
$ cp config/database.yml.example config/database.yml
```

4.Copy and edit Secrets Sample.
Replace the secret_key_base hex string with a new random string (you can generate a new random key with rake secret).

```
$ cp config/secrets.yml.example config/secrets.yml
```

5.Update DB.

```
$ bundle exec rake db:migrate
```

## Running ILL Billing

* WEBrick

```
$ rails s
```

## ILL Billing will be available at

```
http://localhost:3000
```

## Running the Test Suites

```
$ bundle exec rake
$ rubocop
```

## Docs
Various reference documents for this application can be found in the `doc`
folder in this repository.
## Deploying with GitLab ChatOps
This application is setup currently for deployments to be done via GitLab's
chatops feature.

The supported environments to deploy to, via Capistrano, are:
- pontos
- staging
- production

Example: `/gitlab ucsdlibrary/development/billing-ill run production_deploy`

By default, Capistrano will deploy the specified default branch, which is
`master`. If you would like to specify a different branch or tag, you can pass
that as an argument to the slack command.

Example: `/gitlab ucsdlibrary/development/billing-ill run production_deploy 1.5.0`

The above command will deploy the `1.5.0` tag to `production`.

### Scheduling A Production Deployment
The application also supports setting up a GitLab [Scheduled
Pipeline][gitlab-schedule] to support production deployments during the
Operations service window (6-8am PST).

An example schedule using [Cron syntax][cron] might be `30 06 * * 2` which would deploy
the application at 06:30AM on Monday.

Make sure you set the `Cron timezone` in the schedule for `Pacific Time (US and
Canada)`

After the deploy, make sure you de-active or delete the Schedule.
Otherwise the application may accidentally deploy again the following week.

## Rollbacks with GitLab ChatOps
If a deployment gets far enough that it cannot be automatically rolled back, you
can make calls to rollback to the previous release. Internally, this uses the
Capistrano `deploy:rollback` task.

Example: `/gitlab ucsdlibrary/development/billing-ill run production_rollback`

[cron]:https://en.wikipedia.org/wiki/Cron
[gitlab-schedule]:https://docs.gitlab.com/ee/ci/pipelines/schedules.html

